﻿using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using UtilsLib;

namespace VeeamGZIP
{
    class ProcessOptions
    {
        public long FileSize { get; }
        public int BlockCount { get; }
        public int BlockSize { get; }
        public int QueueSize { get; }
        public string SourceFile { get; }
        public string ResultFile { get; }
        public CompressionMode Mode { get; }
        public ProcessOptions(CompressionMode mode, string source, string result)
            : this(mode, source, result, 1024 * 1024, 1024) { }
        public ProcessOptions(CompressionMode mode, string source, string result, int blockSize, int queueSize)
        {
            this.Mode = mode;
            this.SourceFile = source;
            this.ResultFile = result;
            this.FileSize = new FileInfo(source).Length;
            this.BlockSize = this.FileSize > this.BlockSize ? blockSize : (int)this.FileSize;
            this.QueueSize = queueSize;
            this.BlockCount = ((int)FileSize - 1) / this.BlockSize + 1;
        }

        [Conditional("DEBUG")]
        public void Show()
        {
            Logger.Info(
                "Compression Mode: " + this.Mode +
                "\nSource File: " + this.SourceFile +
                "\nResult File: " + this.ResultFile +
                "\nBlock Size: " + this.BlockSize +
                "\nQueue Size: " + this.QueueSize +
                "\nFile Size: " + this.FileSize +
                "\nBlocks Count: " + this.BlockCount);
        }
    }
}

﻿using System;
using System.IO;
using System.IO.Compression;
using TaskManagerLibrary;
using UtilsLib;

namespace VeeamGZIP.Commands
{
    /// <summary>
    /// Write a block of data to a file
    /// </summary>
    class WriteCommand : Command, IDisposable
    {
        public override CommandType Type => CommandType.Protected;
        public PriorityQueue<byte[]> WriteQueue { get; }
        public int WriteIndex { get; private set; }
        private FileStream _stream;
        private ProcessOptions _options;
        public WriteCommand(ProcessOptions options)
        {
            this._options = options;
            this._stream = new FileStream(options.ResultFile, FileMode.Create);
            this.WriteQueue = new PriorityQueue<byte[]>(options.QueueSize);

            if (!this._stream.CanWrite)
                throw new Exception("Can not access result file");
        }
        protected override bool Run()
        {
            if (WriteQueue.Peek().Index == this.WriteIndex)
            {
                Block<byte[]> block = WriteQueue.Dequeue();
                if (this._options.Mode == CompressionMode.Compress)
                {
                    byte[] buffer = BitConverter.GetBytes(block.Data.Length);
                    _stream.Write(buffer, 0, buffer.Length);
                    _stream.Write(block.Data, 0, block.Data.Length);
                }
                else
                {
                    _stream.Write(block.Data, 0, block.Data.Length);
                }
                this.WriteIndex++;
                return true;
            }
            return false;
        }

        public void Dispose() => this._stream.Dispose();
    }
}

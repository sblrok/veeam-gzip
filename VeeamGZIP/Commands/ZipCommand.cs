﻿using System;
using System.IO;
using System.IO.Compression;
using TaskManagerLibrary;
using UtilsLib;

namespace VeeamGZIP.Commands
{
    /// <summary>
    /// Compress or decompress data block
    /// </summary>
    class ZipCommand : Command
    {
        public override CommandType Type => CommandType.Multithread;
        private ProcessOptions _options;
        private PriorityQueue<byte[]> _sourceQueue;
        private PriorityQueue<byte[]> _resultQueue;
        public ZipCommand(ProcessOptions options, PriorityQueue<byte[]> sourceQueue, PriorityQueue<byte[]> resultQueue)
        {
            this._options = options;
            this._sourceQueue = sourceQueue;
            this._resultQueue = resultQueue;

        }
        protected override bool Run()
        {
            Block<byte[]> block = _sourceQueue.Dequeue();
            byte[] result;
            if (this._options.Mode == CompressionMode.Compress)
            {
                using (var compressedStream = new MemoryStream())
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
                {
                    zipStream.Write(block.Data, 0, block.Data.Length);
                    zipStream.Close();
                    result = compressedStream.ToArray();
                }
            }
            else
            {
                using (var compressedStream = new MemoryStream(block.Data))
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
                using (var resultStream = new MemoryStream())
                {
                    zipStream.CopyTo(resultStream);
                    result = resultStream.ToArray();
                }
            }
            Block<byte[]> resultBlock = new Block<byte[]>(result, block.Index);
            this._resultQueue.Enqueue(resultBlock);
            return true;
        }
    }
}

﻿using System;
using System.IO;
using System.IO.Compression;
using TaskManagerLibrary;
using UtilsLib;

namespace VeeamGZIP.Commands
{
    /// <summary>
    /// Read a block of data from file
    /// </summary>
    class ReadCommand : Command, IDisposable
    {
        public override CommandType Type => CommandType.Protected;
        public PriorityQueue<byte[]> ReadQueue { get; }
        public int ReadIndex { get; private set; }
        public bool EOF { get; private set; }
        private FileStream _stream;
        private ProcessOptions _options;
        public ReadCommand(ProcessOptions options)
        {
            this._options = options;
            this._stream = new FileStream(options.SourceFile, FileMode.Open);
            this.ReadQueue = new PriorityQueue<byte[]>(options.QueueSize);

            if (!this._stream.CanRead)
                throw new Exception("Can not access source file");
        }
        protected override bool Run()
        {
            byte[] buffer;
            int size;
            if (this._options.Mode == CompressionMode.Compress)
            {
                buffer = new byte[this._options.BlockSize];
                size = this._stream.Read(buffer, 0, buffer.Length);

                // remove zero bytes
                if (this.ReadIndex == this._options.BlockCount - 1)
                {
                    byte[] copy = new byte[size];
                    Buffer.BlockCopy(buffer, 0, copy, 0, copy.Length);
                    buffer = copy;
                }
            }
            else
            {
                buffer = new byte[4];
                this._stream.Read(buffer, 0, buffer.Length);
                int blockLength = BitConverter.ToInt32(buffer, 0);
                buffer = new byte[blockLength];
                size = this._stream.Read(buffer, 0, buffer.Length);
            }

            if (size == 0)
                this.EOF = true;

            Block<byte[]> block = new Block<byte[]>(buffer, this.ReadIndex);
            this.ReadQueue.Enqueue(block);
            this.ReadIndex++;
            return true;
        }

        public void Dispose() => this._stream.Dispose();
    }
}

﻿using System;
using System.Threading;
using TaskManagerLibrary;
using UtilsLib;
using VeeamGZIP.Commands;

namespace VeeamGZIP
{
    class Core : IDisposable
    {
        private ReadCommand _read;
        private WriteCommand _write;
        private ZipCommand _zip;
        private ManualResetEvent _onEnd;
        private Exception _exception;
        public Core(ProcessOptions options)
        {
            TaskManager.OnFull += OverflowNotification;
            TaskManager.OnError += SendError;
            this._read = new ReadCommand(options);
            this._write = new WriteCommand(options);
            this._zip = new ZipCommand(options, this._read.ReadQueue, this._write.WriteQueue);

            this._read.After += AfterRead;
            this._zip.After += AfterZip;
            this._write.After += AfterWrite;
            this._onEnd = new ManualResetEvent(false);
        }

        public void Start()
        {
            // start workers and add first task to queue
            this._onEnd.Reset();
            this._exception = null;
            TaskManager.Start();
            TaskManager.Enqueue(this._read);
            // waiting until last block is processed
            this._onEnd.WaitOne();

            if (this._exception != null)
                throw this._exception;
        }
        public void Stop() => TaskManager.Stop(true);
        private void SendError(object err)
        {
            this._exception = (Exception)err;
            this._onEnd.Set();
        }
        private void OverflowNotification() => Logger.Warn("Task queue is overflow!");

        public void Dispose()
        {
            this.Stop();
            this._read.Dispose();
            this._write.Dispose();
            this._onEnd.Dispose();
        }
        #region command_interceptors
        private void AfterRead()
        {
            TaskManager.Enqueue(this._zip);
            if (!this._read.EOF)
                TaskManager.Enqueue(this._read);
        }

        private void AfterZip()
        {
            TaskManager.Enqueue(this._write);
        }

        private void AfterWrite()
        {
            if (this._read.EOF && _write.WriteIndex == this._read.ReadIndex)
                this._onEnd.Set();
        }
        #endregion
    }
}

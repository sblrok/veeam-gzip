﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using UtilsLib;

namespace VeeamGZIP
{
    class Program
    {
        private const string HELP_MESSAGE =
            "GZIP Archiver (version 1.0.0)" +
            "\nType 'gzip.exe compress/decompress [input_file_name] [output_file_name]" +
            "\n" +
            "\nCommand list:" +
            "\n-h - show this message" +
            "\n-v - show program version";

        /// <summary>
        /// Multithread block file archiver
        /// </summary>
        /// <param name="args">compress/decompress [input_file_name] [output_file_name]</param>
        static int Main(string[] args)
        {
            Core core = null;
            try
            {
                if (args.Length == 0)
                    throw new Exception(HELP_MESSAGE);
                else if (args.Length < 3)
                {
                    ShowInfo(args);
                    return 1;
                }
                else Validate(args);

                CompressionMode mode = args[0] == "compress" ?
                    CompressionMode.Compress : CompressionMode.Decompress;
                string inputFile = args[1];
                string outputFile = args[2];

                ProcessOptions options = new ProcessOptions(mode, inputFile, outputFile);
                options.Show();

                core = new Core(options);
                core.Start();
                return 1;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                return 0;
            }
            finally
            {
                core?.Dispose();
            }
        }

        private static void ShowInfo(string[] args)
        {
            switch (args[0].ToLower())
            {
                // help commands
                case "-h":
                    Logger.Info(HELP_MESSAGE);
                    break;
                case "-v":
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
                    string version = fileVersionInfo.ProductVersion;
                    Logger.Info(version);
                    break;
            }
        }

        private static void Validate(string[] args)
        {
            if (args.Length != 3)
                throw new Exception(HELP_MESSAGE);
            if (args[0].ToLower() != "compress" && args[0].ToLower() != "decompress")
                throw new Exception("Unknown process mode, type 'compress' or 'decompress'");
            if (!File.Exists(args[1]))
                throw new Exception("Source file doesn`t exist");
            if (File.Exists(args[2]))
                throw new Exception("Result file already exist");
        }
    }
}

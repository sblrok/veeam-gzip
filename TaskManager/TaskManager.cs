﻿using System;
using System.Threading;
using UtilsLib;

namespace TaskManagerLibrary
{
    /// <summary>
    /// Task manager, performs tasks from the queue in multiple threads
    /// </summary>
    public static class TaskManager
    {
        public delegate void MethodHandler();
        public delegate void ParamsMethodHandler(object obj);
        private static Thread[] _threads;
        private static LimitedQueue<Command> _queue;
        public static event MethodHandler OnFull;
        public static event MethodHandler OnEmpty;
        public static event ParamsMethodHandler OnError;
        public static bool Stopped { get; private set; }
        public static bool Safe { get; private set; }
        /// <summary>
        /// Start workers
        /// </summary>
        /// <param name="safe">Indicates whether the workers will stop when an error occurs</param>
        public static void Start(bool safe = true) => Start(Environment.ProcessorCount, 64, safe);
        public static void Start(int threadCount, int queueSize, bool safe)
        {
            Safe = safe;
            _queue = new LimitedQueue<Command>(queueSize);
            _queue.OnFull += TaskManager.NotifyFull;
            _queue.OnEmpty += TaskManager.NotifyEmpty;
            _threads = new Thread[threadCount];
            for (int i = 0; i < _threads.Length; i++)
            {
                Thread worker = new Thread(Worker);
                worker.IsBackground = true;
                worker.Start();
                _threads[i] = worker;
            }
        }
        public static void Stop(bool force = false)
        {
            if (Stopped) return;

            Stopped = true;
            if (force)
            {
                foreach (var thread in _threads)
                    thread.Abort();
            }
            foreach (var thread in _threads)
                thread.Join();
        }
        public static void Enqueue(Command command)
        {
            _queue.Enqueue(command);
        }
        private static void Worker()
        {
            while (!Stopped)
            {
                try
                {
                    Command command = _queue.Dequeue();
                    if (!command.Execute())
                        TaskManager.Enqueue(command);
                }
                catch (ThreadAbortException) { break; }
                catch (Exception e)
                {
                    TaskManager.OnError?.Invoke(e);
                    if (Safe)
                    {
                        Stop(true);
                        break;
                    }
                }
            }
        }

        private static void NotifyFull() => TaskManager.OnFull?.Invoke();
        private static void NotifyEmpty() => TaskManager.OnEmpty?.Invoke();
    }
}
﻿namespace TaskManagerLibrary
{
    public enum CommandType
    {
        /// <summary>
        /// Сommand can run in multiple threads
        /// </summary>
        Multithread,
        /// <summary>
        /// Сommand can run only in single thread
        /// </summary>
        Protected
    }
}

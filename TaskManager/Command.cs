﻿using System.Threading;

namespace TaskManagerLibrary
{
    /// <summary>
    /// Signle command for task queue
    /// </summary>
    public abstract class Command
    {
        private object _locker = new object();
        public delegate void MethodHandler();
        public event MethodHandler Before;
        public event MethodHandler After;
        public abstract CommandType Type { get; }
        public bool CanExecute
        {
            get
            {
                if (Type == CommandType.Protected)
                    return !Monitor.IsEntered(_locker);
                else if (Type == CommandType.Multithread)
                    return true;
                return false;
            }
        }
        public bool Execute()
        {
            if (Type == CommandType.Protected)
            {
                if (Monitor.TryEnter(_locker))
                {
                    try
                    {
                        this.Before?.Invoke();
                        return Run();
                    }
                    finally
                    {
                        this.After?.Invoke();
                        Monitor.Exit(_locker);
                    }
                }
            }
            else if (Type == CommandType.Multithread)
            {
                this.Before?.Invoke();
                try
                {
                    return Run();
                }
                finally
                {
                    this.After?.Invoke();
                }
            }

            return false;
        }
        protected abstract bool Run();
    }
}
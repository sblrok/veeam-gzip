﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace UtilsLib
{
    /// <summary>
    /// Queue with a limited number of items
    /// </summary>
    /// <typeparam name="T">Items type</typeparam>
    public class LimitedQueue<T> : IEnumerable<T>
    {
        protected Queue<T> _queue;
        public delegate void MethodHandler();
        public event MethodHandler OnFull;
        public event MethodHandler OnEmpty;
        public int MaxCount { get; }
        public int Count => _queue.Count;
        public bool IsFull => Count == MaxCount;
        public bool IsEmpty => Count == 0;
        public LimitedQueue(int maxCount)
        {
            MaxCount = maxCount;
            _queue = new Queue<T>(maxCount);
        }
        public virtual void Enqueue(T item)
        {
            Monitor.Enter(_queue);

            try
            {
                while (this.IsFull)
                    Monitor.Wait(_queue);

                _queue.Enqueue(item);

                if (this.IsFull)
                    this.OnFull?.Invoke();

                if (this._queue.Count == 1)
                    Monitor.PulseAll(_queue);
            }
            finally
            {
                Monitor.Exit(_queue);
            }
        }
        public virtual bool TryEnqueue(T item, int waitTime)
        {
            if (Monitor.TryEnter(_queue, waitTime) && !IsFull)
            {
                try
                {
                    _queue.Enqueue(item);

                    if (this.IsFull)
                        this.OnFull?.Invoke();

                    if (this._queue.Count == 1)
                        Monitor.PulseAll(_queue);
                }
                finally
                {
                    Monitor.Exit(_queue);
                }
                return true;
            }
            return false;
        }

        public virtual T Dequeue()
        {
            T item;

            Monitor.Enter(_queue);

            try
            {
                while (this.IsEmpty)
                    Monitor.Wait(_queue);

                item = _queue.Dequeue();

                if (this.IsEmpty)
                    this.OnEmpty?.Invoke();

                if (_queue.Count == MaxCount - 1)
                    Monitor.PulseAll(_queue);
            }
            finally
            {
                Monitor.Exit(_queue);
            }

            return item;
        }

        public virtual bool TryDequeue(out T item, int waitTime)
        {
            item = default(T);
            if (Monitor.TryEnter(_queue, waitTime) && !IsEmpty)
            {
                try
                {
                    item = _queue.Dequeue();

                    if (this.IsEmpty)
                        this.OnEmpty?.Invoke();

                    if (_queue.Count == MaxCount - 1)
                        Monitor.PulseAll(_queue);
                }
                finally
                {
                    Monitor.Exit(_queue);
                }
                return true;
            }
            return false;
        }
        public virtual T Peek() => _queue.Peek();
        public IEnumerator<T> GetEnumerator() => _queue.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}

﻿namespace UtilsLib
{
    public enum LogType
    {
        Info,
        Warn,
        Error
    }
}
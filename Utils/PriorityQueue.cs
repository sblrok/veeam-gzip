﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace UtilsLib
{
    /// <summary>
    /// Limited queue with sorted blocks of data
    /// </summary>
    public class PriorityQueue<T> : IEnumerable<Block<T>>
    {
        protected List<Block<T>> _list;
        public int MaxCount { get; }
        public int StartIndex { get; }
        public int Count => _list.Count;
        public bool IsFull => Count == MaxCount;
        public bool IsEmpty => Count == 0;
        public PriorityQueue(int maxCount, int startIndex = 0)
        {
            MaxCount = maxCount;
            StartIndex = startIndex;
            _list = new List<Block<T>>(maxCount);
        }
        public virtual void Enqueue(Block<T> item)
        {
            Monitor.Enter(_list);

            try
            {
                while (this.IsFull)
                    Monitor.Wait(_list);

                int insertIndex = this._list.BinarySearch(item);
                insertIndex = insertIndex >= 0 ? insertIndex : -insertIndex - 1;
                this._list.Insert(insertIndex, item);


                if (this._list.Count == 1)
                    Monitor.PulseAll(this._list);
            }
            finally
            {
                Monitor.Exit(_list);
            }
        }
        public virtual bool TryEnqueue(Block<T> item, int waitTime)
        {
            if (Monitor.TryEnter(_list, waitTime) && !IsFull)
            {
                try
                {
                    int insertIndex = this._list.BinarySearch(item);
                    insertIndex = insertIndex >= 0 ? insertIndex : -insertIndex - 1;
                    this._list.Insert(insertIndex, item);

                    if (this._list.Count == 1)
                        Monitor.PulseAll(this._list);
                }
                finally
                {
                    Monitor.Exit(_list);
                }
                return true;
            }
            return false;
        }

        public virtual Block<T> Dequeue()
        {
            Block<T> item;
            Monitor.Enter(_list);

            try
            {
                while (this.IsEmpty)
                    Monitor.Wait(_list);

                item = this._list[0];
                this._list.RemoveAt(0);

                if (this._list.Count == this.MaxCount - 1)
                    Monitor.PulseAll(this._list);
            }
            finally
            {
                Monitor.Exit(_list);
            }

            return item;
        }

        public virtual bool TryDequeue(out Block<T> item, int waitTime)
        {
            item = default;
            if (Monitor.TryEnter(_list, waitTime) && !IsEmpty)
            {
                try
                {
                    item = this._list[0];
                    this._list.RemoveAt(0);

                    if (this._list.Count == this.MaxCount - 1)
                        Monitor.PulseAll(this._list);
                }
                finally
                {
                    Monitor.Exit(_list);
                }
                return true;
            }
            return false;
        }
        public virtual Block<T> Peek()
        {
            Block<T> item;
            Monitor.Enter(_list);

            try
            {
                while (this.IsEmpty)
                    Monitor.Wait(_list);

                item = this._list[0];
            }
            finally
            {
                Monitor.Exit(_list);
            }

            return item;
        }
        public IEnumerator<Block<T>> GetEnumerator() => _list.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}

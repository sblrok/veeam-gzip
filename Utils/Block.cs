﻿using System;

namespace UtilsLib
{
    /// <summary>
    /// Block of data
    /// </summary>
    /// <typeparam name="T">Type of data in the block</typeparam>
    public class Block<T> : IComparable<Block<T>>
    {
        public T Data { get; set; }
        public int Index { get; set; }
        public Block(T data, int index)
        {
            this.Data = data;
            this.Index = index;
        }
        public int CompareTo(Block<T> other)
        {
            if (this.Index > other.Index) return 1;
            if (this.Index < other.Index) return -1;
            return 0;
        }
    }
}

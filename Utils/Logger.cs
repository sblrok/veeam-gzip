﻿using System;
using System.Threading;

namespace UtilsLib
{
    /// <summary>
    /// Thread-safe logger
    /// </summary>
    public class Logger
    {
        private static object _locker = new object();
        public static void Show(LogType type, string message)
        {
            Monitor.Enter(_locker);

            switch (type)
            {
                case LogType.Warn:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }

            Console.WriteLine($"[{DateTime.Now}] {message}");
            Console.ResetColor();

            Monitor.Exit(_locker);
        }

        public static void Info(string message) => Show(LogType.Info, message);
        public static void Warn(string message) => Show(LogType.Warn, message);
        public static void Error(string message) => Show(LogType.Error, message);
    }
}

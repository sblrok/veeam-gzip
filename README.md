# veeam-gzip

**Block compression/decompression application**

To compress a file, you must call the application as follows:  
```
VeeamGZIP.exe compress "source_file" "result_file"
```
**Application allows you to decompress files created only by this application**  

To decompress:
```
VeeamGZIP.exe decompress "source_file" "result_file"
```

For more information please contact me by mail: dimasblrok@gmail.com